package com.example.simpleshop.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "review")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Review extends CommonAbstractModel{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id", foreignKey = @ForeignKey(name = "fk_rate_review"))
    @ToString.Exclude
    private Item item;

    private String text;
}
package com.example.simpleshop.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "purchase")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Purchase extends CommonAbstractModel {
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    private Item item;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "consumer_id", foreignKey = @ForeignKey(name = "fk_purchase_consumer"))
    private SystemUser consumer;
}

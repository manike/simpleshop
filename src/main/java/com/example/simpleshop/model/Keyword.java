package com.example.simpleshop.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "keyword")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Keyword extends CommonAbstractModel{
    @ManyToMany(mappedBy = "keywords", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    private Set<Item> item = new HashSet<>();

    private String word;
}
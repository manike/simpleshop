package com.example.simpleshop.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "organization")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Organization extends CommonAbstractModel {
    private String name;
    private String description;
    private String logo;

    @OneToMany(mappedBy = "organization", fetch = FetchType.LAZY)
    @ToString.Exclude
    private Set<Item> items = new HashSet<>();
}
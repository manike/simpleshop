package com.example.simpleshop.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "discount")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Discount extends CommonAbstractModel{
    @ManyToMany(mappedBy = "discounts", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    private Set<Item> items = new HashSet<>();

    private Double amount;

    private LocalDate startDate;

    private LocalDate endDate;

    private String description;
}
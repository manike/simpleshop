package com.example.simpleshop.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "item")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Item extends CommonAbstractModel {
    private String name;
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id", foreignKey = @ForeignKey(name = "fk_item_organization"))
    private Organization organization;

    private BigDecimal price;
    private int amount;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "item_discounts",
            joinColumns = @JoinColumn(name = "item_id"), foreignKey = @ForeignKey(name = "fk_items_discounts"),
            inverseJoinColumns = @JoinColumn(name = "discount_id"), inverseForeignKey = @ForeignKey(name = "fk_discounts_items"))
    @ToString.Exclude
    private Set<Discount> discounts = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "item")
    @ToString.Exclude
    private Set<Review> reviews = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "item_keyword",
                joinColumns = @JoinColumn(name = "item_id"), foreignKey = @ForeignKey(name = "fk_items_keywords"),
                inverseJoinColumns = @JoinColumn(name = "keyword_id"), inverseForeignKey = @ForeignKey(name = "fk_keywords_items"))
    @ToString.Exclude
    private Set<Keyword> keywords = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "specification_id")
    @ToString.Exclude
    private ItemSpecification specification;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "item")
    @ToString.Exclude
    private Set<Rating> rates = new HashSet<>();
}

package com.example.simpleshop.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "system_user")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SystemUser extends CommonAbstractModel {
    private String login;
    private String email;
    private String password;
    private BigDecimal balance;

    @OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY)
    @ToString.Exclude
    private Set<Notification> receivedNotifications = new HashSet<>();

    @OneToMany(mappedBy = "sender", fetch = FetchType.LAZY)
    @ToString.Exclude
    private Set<Notification> adminSentNotifications = new HashSet<>();

    @OneToMany(mappedBy = "consumer", fetch = FetchType.LAZY)
    @ToString.Exclude
    private Set<Purchase> purchases = new HashSet<>();

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "FK_USER_ROLES"), nullable = false)
    private Role role;
}

package com.example.simpleshop.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "item_specification")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ItemSpecification extends CommonAbstractModel {
    private int width;
    private int length;
    private int height;
    private int weight;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id")
    @ToString.Exclude
    private Item item;
}
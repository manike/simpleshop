package com.example.simpleshop.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "notification")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Notification extends CommonAbstractModel{
    private String noteHeader;
    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sender_id", foreignKey = @ForeignKey(name = "fk_notification_sender"))
    @ToString.Exclude
    private SystemUser sender;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "receiver_id", foreignKey = @ForeignKey(name = "fk_notification_receiver"))
    @ToString.Exclude
    private SystemUser receiver;
}
